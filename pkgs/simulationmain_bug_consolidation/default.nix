
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulationmain";
  version = "0.1.0";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "0ab07abd6937d21c858ad6167438d745a42b3fbb";
    sha256 = "1nxx9q8la0n4s8pmsldgpamh8gfgbqvpcsh1vfrh0y94xymh9rbd";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}