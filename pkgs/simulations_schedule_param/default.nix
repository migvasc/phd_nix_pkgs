
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulations_schedule_param";
  version = "1.1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "155f51ed61eac6d32668e8f7bca7b35adfb5d12a";
    sha256 = "sha256-5C0pukt5+gQ9HqDzSU2G2aoqQhZ38/Rx5N6VRXNJ668=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}