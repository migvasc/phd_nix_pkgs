
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulations_batteries";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "0a9b66c3b65fae2fbe05d7a8d8278e5b56500014";
    sha256 = "sha256-3BR8FZlghmQeccnD/Jq5hN+d/0g5M3cfLy74snYGdus=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}