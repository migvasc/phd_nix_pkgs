
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulationmain";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "0455560dae1691568170ca51f14ba93e8be9e389";
    sha256 = "08ca6gfzhw6y6qp8dbvppdzl86fy2495yic2gbky6fiv6fr2wx81";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}