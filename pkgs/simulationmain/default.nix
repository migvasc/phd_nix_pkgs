
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulationmain";
  version = "0.1.0";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "8459ddd8a8f46c25a5d4b9cffbbdae1b228d1a53";
    sha256 = "1jiskvsh69q1xgcvkivby8f7zcza9x0wjl52bzak8qr7n1g38k3j";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}