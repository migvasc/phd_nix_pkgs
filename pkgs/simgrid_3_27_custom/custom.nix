{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-3.27_mig";
  rev = "c3a11820c6a16aa9a7b9b0ad7eea8071ca54d202"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://gitlab.inria.fr/msilvava/custom_simgrid_3.27/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "190fbj9p4zxgjq50wrp34dhavgmi6a7hxr21cwgc46979yckns1y";
  };
})
