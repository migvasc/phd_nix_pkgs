
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulationmain";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "ea7859a5d9d970ceda6a2734111d91fbaf7fd11a";
    sha256 = "0b40mpkc2xjf3sllfxgprnx140vvrlf65dxyjl0jmz8w2wjp1sz7";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}