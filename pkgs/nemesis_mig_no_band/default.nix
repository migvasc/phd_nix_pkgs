
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "nemesis";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "3e651e6658f0146024e82802aed6e7a92c257656";
    sha256 = "sha256-dZRgKmK5Du+l7tZEQhPKscEQ7f86mjVDdziypWTMuP8=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}