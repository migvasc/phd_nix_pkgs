{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-3_26_mig";
  rev = "8b7ea6731538a0a76d94f0d7138c605cca879437"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://gitlab.inria.fr/msilvava/custom_simgrid_3.26/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "10khmwnqyzghnz5lcwpfrxzx1fbmdl1nzx560hwnq7l9hfs2hyb9";
  };
})
