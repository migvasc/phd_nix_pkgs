
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "migrations_no_congestion";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/perform_vm_migrations_isolated";
    rev = "0d92520a4f1cf4a93797e525a105e755e92c0291";
    sha256 = "sha256-mVn2EDm6rmUoTXQRcPcE2OS7B/WhLgOUPvzWX+b7CZY=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}