
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "simulationmain";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "c6efea49612836b1d98cbb0538a33a27c4d273a5";
    sha256 = "1pi1pab09sc7qqwvbf4hybhai55f74v9l88qsxgfwa0r1pf392n3";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}