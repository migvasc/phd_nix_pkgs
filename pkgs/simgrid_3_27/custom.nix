{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-3.27";
  rev = "a585144d870def9053b47ed0f5cd92ac337038a5"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://framagit.org/simgrid/simgrid/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "11ail329958vpiq85yp1q8aymwcigvihjflc14yjkqlsj9bh8dyb";
  };
})