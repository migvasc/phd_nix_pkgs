{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-3.28_mig";
  rev = "86ce36d5253d26b7c7057ea10230537560118f10"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://gitlab.inria.fr/msilvava/custom_simgrid_3.28/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "sha256-DgU17ocCqWnvVDUNrngJhT/lq/z76GxHOA27c4f9z6o=";
  };
})
