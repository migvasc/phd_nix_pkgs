{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-3.31_custom_mig";
  rev = "27e56b80a3e8a6cdf2ccc99cf62126e156e8d8c4"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://gitlab.inria.fr/msilvava/custom_simgrid_3.31/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "sha256-9wOpOvw+bUfXJpMYQdtkeBZ7nwo36iNq1Co/gkbSaq4=";
  };
})
