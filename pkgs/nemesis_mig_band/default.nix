
{ stdenv, fetchgit, cmake, simgrid, boost }:

stdenv.mkDerivation rec {
  pname = "nemesis";
  version = "1";

  src = fetchgit {
    url = "https://gitlab.com/migvasc/simgrid-first-prototype";
    rev = "0edf969f629e9fe136a154dbb7dbe35570dfdb4d";
    sha256 = "sha256-BD2YCBksxT95i5ltRR21AP6Dpi5J8q0SAO8unMZFzVM=";
  };

  buildInputs = [
    cmake
    simgrid
    boost
  ];
}