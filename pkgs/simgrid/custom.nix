{ simgrid, fetchurl } :

simgrid.overrideAttrs(oldAttrs: rec {
  version = oldAttrs.version + "-custom";
  rev = "a1b891dc26d75c259f8080da805bebe7ff5f76f6"; # The desired SimGrid commit.
  src = fetchurl {
    url = "https://framagit.org/simgrid/simgrid/-/archive/${rev}/simgrid-${rev}.tar.gz";
    sha256 = "026maiadg0skp04lw1p2pkfhgscmzxhap8cqirmv3cfaa4h6rlkf";
  };
})