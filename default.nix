{
  pkgs ? import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/b58ada326aa612ea1e2fb9a53d550999e94f1985.tar.gz) {}
}:

with pkgs;



let
  packages = rec {
    migrations_no_congestion = callPackage ./pkgs/migrations_no_congestion {simgrid = simgrid_3_28_custom;};    
    simulationmain_performance = callPackage ./pkgs/simulationmain_performance {simgrid = simgrid_3_27_custom;};    
    simulations_schedule_param = callPackage ./pkgs/simulations_schedule_param {simgrid = simgrid_3_28_custom;};
    simulations_batteries = callPackage ./pkgs/simulations_batteries {simgrid = simgrid_3_28_custom;};
    nemesis_mig_band = callPackage ./pkgs/nemesis_mig_band {simgrid = simgrid_3_28_custom;};
    nemesis_mig_no_band = callPackage ./pkgs/nemesis_mig_no_band {simgrid = simgrid_3_28_custom;};    
    simulationmain_fixed_index = callPackage ./pkgs/simulationmain_fixed_index {simgrid = simgrid_3_27_custom;};
    simulationmain_fixed = callPackage ./pkgs/simulationmain_fixed {simgrid = simgrid_3_27_custom;};
    simulationmain = callPackage ./pkgs/simulationmain {};
    simulationmain_bug_consolidation_sg_3_27 = callPackage ./pkgs/simulationmain_bug_consolidation {  simgrid = simgrid_3_27;  };
    simulationmain_sg_3_27 = callPackage ./pkgs/simulationmain { simgrid = simgrid_3_27;  };
    simulationmain_bug_consolidation = callPackage ./pkgs/simulationmain_bug_consolidation {  simgrid = simgrid_3_27_custom;  };
    simulationmain_custom_sg = callPackage ./pkgs/simulationmain { simgrid = custom_simgrid; };
    custom_simgrid = callPackage ./pkgs/simgrid/custom.nix {};
    simgrid_3_27 = callPackage ./pkgs/simgrid_3_27/custom.nix {};
    simgrid_3_27_custom = callPackage ./pkgs/simgrid_3_27_custom/custom.nix {};
    simgrid_3_28_custom = callPackage ./pkgs/simgrid_3_28_custom/custom.nix {};    
    simgrid_3_31_custom = callPackage ./pkgs/simgrid_3_31_custom/custom.nix {};    
inherit pkgs; # similar to `pkgs = pkgs;` This lets callers use the nixpkgs version defined in this file.

};
in
  packages
